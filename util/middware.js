function authMiddware(req, res, next)  {
    const access_token = req.headers.authorization.split(' ')[1]
    console.log(access_token);
    if (access_token != null) {
      console.log('have no token');
      return res.sendStatus(401)
    }
    else {
      console.log('have token');
      next()
    }
}

function logMiddware(err, req, res, next) {
  console.log(err)
  res.sendStatus(500)
  throw new Error(err)
}


authMiddware.prototype.unless = require('express-unless')

module.exports = {
    authMiddware,
    logMiddware
}