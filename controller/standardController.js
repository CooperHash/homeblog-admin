var dbConfig = require('../util/dbconfig');

getStandard = (req, res) => {
  var sql = 'select id,img,info,title,content from standard'
  var sqlArr = [];
  var callback = (err, data) => {
    if (err) {
      console.log('连接出错');
      console.log(err);
    } else {
      return res.send({
        'data': data,
      })
    }
  }
  dbConfig.sqlConnect(sql, sqlArr, callback);
}


getSingleStandard = (req, res) => {
  const { id } = req.query
  var sql = 'select id,img,info,title,content,translate from standard where id=?'
  var sqlArr = [id];
  var callback = (err, data) => {
    if (err) {
      console.log('连接出错');
      console.log(err);
    } else {
      return res.send({
        'data': data,
      })
    }
  }
  dbConfig.sqlConnect(sql, sqlArr, callback);
}


saveTranslate = (req, res) => {
  const params = req.body
  const sql = 'UPDATE standard SET translate=? WHERE id=?'
  const sqlArr = [params.translate,params.id]
  const callBack = (err ,data) => {
    if(err) {
      console.log('出错');
      console.log(err);
    } else {
      return res.send({
        'message': 'success'
      })
    }
  }
  dbConfig.sqlConnect(sql, sqlArr, callBack)
}


module.exports = {
  getStandard,
  saveTranslate,
  getSingleStandard
}
