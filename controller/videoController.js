var dbConfig = require("../util/dbconfig");

getAllVideo = (req, res) => {
  var sql =
    "select vid,vurl,vimage,vname,vguy from video left join person on video.vguy = person.name;";
  var sqlArr = [];
  var callback = (err, data) => {
    if (err) {
      console.log("连接出错");
      console.log(err);
    } else {
      return res.send({
        data: data,
      });
    }
  };
  dbConfig.sqlConnect(sql, sqlArr, callback);
};

getVideo = (req, res) => {
  let { vid } = req.query;
  var sql = "select vid, vname,vurl,vimage,vguy from video where vid=?";
  var sqlArr = [vid];
  var callback = (err, data) => {
    if (err) {
      console.log("连接出错");
      console.log(err);
    } else {
      return res.send(data[0]);
    }
  };
  dbConfig.sqlConnect(sql, sqlArr, callback);
};

getVideoListByType = (req, res) => {
  let { vtype } = req.query;
  var sql = "select * from video where vtype=?";
  var sqlArr = [vtype];
  var callback = (err, data) => {
    if (err) {
      console.log("连接出错");
      console.log(err);
    } else {
      return res.send(data);
    }
  };
  dbConfig.sqlConnect(sql, sqlArr, callback);
};

module.exports = {
  getAllVideo,
  getVideo,
  getVideoListByType,
};
