var dbConfig = require('../util/dbconfig');
runCode = (req, res) => {
  var params = req.body
  res.send(
    `<!DOCTYPE html>
      <html>
      
      <head>
        <style>
      
        </style>
        <script src="https://stacksnippets.net/scripts/snippet-javascript-console.min.js?v=1"></script>
      </head>
      
      <body>
      
        <script type="text/javascript">
          ${params.js}
        </script>
      </body>
      
      </html>`
  )
}


saveCode = (req, res) => {
  var params = req.body
  var sql = 'insert into code(code, info, tag) values (?,?,?); ALTER TABLE code AUTO_INCREMENT = 1;'
  var sqlArr = [params.code, params.info, params.tag];
  var callback = (err, data) => {
    if (err) {
      console.log('连接出错');
      console.log(err);
    } else {
      return res.send({
        'message': 'success',
      })
    }
  }
  dbConfig.sqlConnect(sql, sqlArr, callback);
}

getCode = (req, res, next) => {
  var sql = 'select id,code,tag,info from code'
  var sqlArr = []
  var callback = (err, data) => {
    if (err) {
      console.log('连接出错');
      next(err)
    } else {
      return res.send({
        'data': data,
      })
    }
  }
  dbConfig.sqlConnect(sql, sqlArr, callback);
}

getSingleCode = (req, res) => {
  var params = req.query
  var sql = 'select * from code where id=?'
  var sqlArr = [params.id]
  var callback = (err, data) => {
    if (err) {
      console.log('连接出错');
      console.log(err);
    } else {
      return res.send({
        'data': data,
      })
    }
  }
  dbConfig.sqlConnect(sql, sqlArr, callback);
}


updateCode = (req, res) => {
  const params = req.body
  const sql = 'UPDATE code SET code=? WHERE id=?;ALTER TABLE code AUTO_INCREMENT = 1;'
  const sqlArr = [params.code, params.id]
  const callBack = (err, data) => {
    if (err) {
      console.log('出错');
      console.log(err);
    } else {
      return res.send({
        'message': 'success'
      })
    }
  }
  dbConfig.sqlConnect(sql, sqlArr, callBack)
}

deleteCode = (req, res) => {
  const params = req.body
  const sql = 'delete from code where id=?;ALTER TABLE code AUTO_INCREMENT = 1;'
  const sqlArr = [params.id]
  const callBack = (err, data) => {
    if (err) {
      console.log('出错');
      console.log(err);
    } else {
      return res.send({
        'message': 'success'
      })
    }
  }
  dbConfig.sqlConnect(sql, sqlArr, callBack)
}

module.exports = {
  deleteCode,
  runCode,
  saveCode,
  getCode,
  getSingleCode,
  updateCode
}
