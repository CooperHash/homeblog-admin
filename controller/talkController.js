var dbConfig = require("../util/dbconfig");

getAllTalk = (req, res) => {
  var sql = "select id,name,content,img,title from talk";
  //"select a.id as id,a.name as name,a.content as content,b.img as img from talk as a left join person as b on a.name=b.name";
  var sqlArr = [];
  var callback = (err, data) => {
    if (err) {
      console.log("连接出错");
      console.log(err);
    } else {
      return res.send({
        data: data,
      });
    }
  };
  dbConfig.sqlConnect(sql, sqlArr, callback);
};

getTalk = (req, res) => {
  const { id } = req.query;
  const sql = "select id,name,content,img,title from talk where id=?";
  const sqlArr = [id];
  const callBack = (err, data) => {
    if (err) {
      console.log("连接出错");
      console.log(err);
    } else {
      return res.json(data[0]);
    }
  };
  dbConfig.sqlConnect(sql, sqlArr, callBack);
};

saveTalk = (req, res) => {
  var params = req.body;
  var sql =
    "ALTER TABLE talk AUTO_INCREMENT = 1;insert into talk(name,content) values (?,?);";
  var sqlArr = [params.name, params.content];
  var callBack = (err) => {
    if (err) {
      console.log("连接出错");
      console.log(err);
    } else {
      return res.send({ msg: "sccess" });
    }
  };

  dbConfig.sqlConnect(sql, sqlArr, callBack);
};

deleteTalk = (req, res) => {
  var params = req.body;
  var sql = "delete from talk where id=?;ALTER TABLE talk AUTO_INCREMENT = 1;";
  var sqlArr = [params.id];
  var callBack = (err) => {
    if (err) {
      console.log("连接出错");
      console.log(err);
    } else {
      return res.send({ msg: "sccess" });
    }
  };

  dbConfig.sqlConnect(sql, sqlArr, callBack);
};

module.exports = {
  getAllTalk,
  getTalk,
  saveTalk,
  deleteTalk,
};
