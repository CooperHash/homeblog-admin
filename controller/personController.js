const dbConfig = require('../util/dbconfig')

getPerson = (req, res) => {
  const { name } = req.query
  const sql = 'select * from person where name=?';
  const sqlArr = [name];
  const callback = (err, data) => {
    if (err) {
      console.log('连接出错');
      console.log(err);
    } else {
      return res.send({
        'data': data
      });
    }
  }

  dbConfig.sqlConnect(sql, sqlArr, callback);
}


module.exports = {
  getPerson
}