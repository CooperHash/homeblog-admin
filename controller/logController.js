var dbConfig = require('../util/dbconfig');

saveLog = (req, res) => {
  var params = req.body
  var sql = 'ALTER TABLE log AUTO_INCREMENT = 1; insert into log(time,payload) values (?,?);'
  var sqlArr = [params.time,params.payload];
  var callback = (err, data) => {
    if (err) {
      console.log('连接出错');
      console.log(err);
    } else {
      return res.send({
        'message': 'success',
      })
    }
  }
  dbConfig.sqlConnect(sql, sqlArr, callback);
}



module.exports = {
  saveLog
}
