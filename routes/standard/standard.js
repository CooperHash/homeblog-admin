var express = require('express');
var router = express.Router();
var standard = require('../../controller/standardController')
/* GET home page. */
router.get('/api/getStandard',standard.getStandard);
router.get('/api/getSingleStandard', standard.getSingleStandard)
router.put('/api/saveTranslate',standard.saveTranslate)
module.exports = router;