var express = require('express');
var router = express.Router();
var talk = require('../../controller/talkController')
/* GET home page. */
router.get('/api/getAllTalk',talk.getAllTalk)
router.get('/api/getTalk',talk.getTalk);
router.post('/api/saveTalk',talk.saveTalk)
router.post('/api/deleteTalk',talk.deleteTalk)
module.exports = router;