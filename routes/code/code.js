var express = require('express');
var router = express.Router();
var code = require('../../controller/codeController')
/* GET home page. */
router.post('/api/runCode',code.runCode)
router.post('/api/saveCode',code.saveCode)
router.get('/api/getCode',code.getCode)
router.get('/api/getSingleCode',code.getSingleCode)
router.put('/api/updateCode',code.updateCode)
router.post('/api/deleteCode',code.deleteCode)
module.exports = router;