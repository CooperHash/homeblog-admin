var express = require('express');
var router = express.Router();
var video = require('../../controller/videoController')
/* GET home page. */
router.get('/api/getAllVideo',video.getAllVideo);
router.get('/api/getVideo',video.getVideo);
router.get('/api/getVideoListByType',video.getVideoListByType)
module.exports = router;