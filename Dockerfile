FROM node:16
WORKDIR /www/wwwroot/backen
COPY . /www/wwwroot/backen
RUN npm install
EXPOSE 3010
CMD [ "node" "app.js" ]
