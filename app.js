const jsonwebtoken = require("jsonwebtoken");
const bodyParser = require("body-parser");
const unless = require("express-unless");
const express = require("express");
const path = require("path");
const dbConfig = require("./util/dbconfig");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const blogRouter = require("./routes/blog/blog");
const japanRouter = require("./routes/language/japan");
const recentRouter = require("./routes/recent/recent");
const bookRouter = require("./routes/books/books");
const userRouter = require("./routes/user/user");
const poemRouter = require("./routes/poem/poem");
const kpopRouter = require("./routes/kpop/kpop");
const videoRouter = require("./routes/video/video");
const standardRouter = require("./routes/standard/standard");
const personRouter = require("./routes/person/person");
const talkRouter = require("./routes/talk/talk");
const codeRouter = require("./routes/code/code");
const logRouter = require("./routes/log/log");
const qiniuRouter = require("./routes/qiniu/qiniu");
// 进行解密
// const jwt = require('express-jwt');
const SECRET_KEY = "login2021";
const app = express();
const http = require("http");
const { logMiddware } = require("./util/middware");
const server = http.createServer(app);

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

// app.use(jwt({ secret: SECRET_KEY, algorithms: ['HS256']})
// .unless({path: ['/all/user/api/saveUser']}));

app.use("/all/blog", blogRouter);
app.use("/all/language", japanRouter);
app.use("/all/recent", recentRouter);
app.use("/all/book", bookRouter);
app.use("/all/user", userRouter);
app.use("/all/poem", poemRouter);
app.use("/all/kpop", kpopRouter);
app.use("/all/video", videoRouter);
app.use("/all/talk", talkRouter);
app.use("/all/code", codeRouter);
app.use("/all/log", logRouter);
app.use("/all/qiniu", qiniuRouter);

app.use("/all/standard", standardRouter);
// (req, res, next) => {
//   if (req.headers.authorization != undefined) {
//     console.log(req.headers.authorization);
//     const access_token = req.headers.authorization.split(' ')[1]
//     const decode_token = jsonwebtoken.verify(access_token, SECRET_KEY)
//     console.log(decode_token);
//     next()
//   } else {
//     console.log('gg');
//     return res.sendStatus(401)
//   }
// },

app.use("/all/person", personRouter);

app.get("/all/user/api/login", (req, res) => {
  const { username, userpassword } = req.query;
  const sql = "select userpassword from user where username=?";
  const sqlArr = [username];
  const callback = (err, data) => {
    if (err) {
      console.log("连接出错");
      console.log(err);
    } else {
      console.log("user", username);
      console.log("password", userpassword);
      const user = Object.values(JSON.parse(JSON.stringify(data)));
      console.log(user[0]);

      if (user[0].userpassword == userpassword) {
        const token = jsonwebtoken.sign(
          {
            username: user[0].username,
            userpassword: user[0].userpassword,
          },
          SECRET_KEY,
          {
            algorithm: "HS256",
            expiresIn: "30 days",
          }
        );
        console.log("token ", token);
        return res.send({
          message: true,
          token: token,
        });
      } else {
        return res.sendStatus(404).send({
          message: false,
        });
      }
    }
  };
  dbConfig.sqlConnect(sql, sqlArr, callback);
});

// 设置响应头
app.all("*", function (req, res) {
  res.header("Access-Control-Allow-Origin", "http://localhost:5173");
  res.header(
    "Access-Control-Allow-Headers",
    "X-resuested-With,Authorization,Origin"
  );
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
});

server.listen("3010");
